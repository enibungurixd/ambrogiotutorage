import * as ORM from 'sequelize';
import { keys } from '../keys/keys'
//import model functions to initialize them
import {userModel} from './UserModel';
import {pathModel} from './PathModel';
import {reportModel} from './ReportModel';
import {roleModel} from './RoleModel';
import {studentModel} from './StudentModel';
import {taskModel} from './TaskModel';
import {tutorModel} from './TutorModel';


//connection of sequelize with postgres DB
export const sequelize = new ORM(keys.postgresURI);
//authentication and after a console log message case successful or unsuccessful
sequelize.authenticate().then(()=> {
    console.log('Connection to database has been established successfully.');
}).catch(err => {
    console.error('Unable to connect to the database: ', err);
});
//export constants for initializing sequelize models
export const UserModel = userModel(sequelize);
export const PathModel = pathModel(sequelize);
export const ReportModel = reportModel(sequelize);
export const RoleModel = roleModel(sequelize);
export const StudentModel = studentModel(sequelize);
export const TutorModel = tutorModel(sequelize);
export const TaskModel = taskModel(sequelize)

//Relations between tables         
UserModel.belongsTo(PathModel, {as: 'course', foreignKey: 'pathFK'});

ReportModel.belongsTo(UserModel, {as: 'report', foreignKey: 'userFK'})

UserModel.belongsTo(RoleModel, {foreignKey: 'roleFK',   as: 'role'})

UserModel.belongsTo(StudentModel, {foreignKey: 'userID', as: 'userfk' });

ReportModel.belongsToMany(StudentModel, {as: 'reports', through: 'Student_Report', foreignKey: 'reportID', otherKey: 'reportFK'});

TutorModel.belongsToMany(ReportModel, {as: 'reports', through: 'Tutor_Report', foreignKey: 'reportID', otherKey: 'reportID'});

ReportModel.belongsToMany(TutorModel, {as: 'tutors', foreignKey: 'tutorID', through: 'Tutor_Report',otherKey: 'tutorID'});

TutorModel.belongsTo(UserModel, {foreignKey: 'userID', as: 'userfk' });

TaskModel.belongsTo(PathModel, {foreignKey: 'pathFK', as: 'path'})

StudentModel.belongsToMany(TaskModel, {as: 'student', through: 'Student_Task', foreignKey: 'studentID', otherKey: 'studentFK'});

TaskModel.belongsToMany(StudentModel, {as: 'task', through: 'Student_Task', foreignKey: 'taskID', otherKey: 'taskFK'});

TutorModel.belongsToMany(TaskModel, {as: 'tutors', through: 'Tutor_Task', foreignKey: 'tutorID', otherKey: 'tutorFK'});

TaskModel.belongsToMany(TutorModel, {as: 'tasks', through: 'Tutor_Task', foreignKey: 'taskID', otherKey: 'taskFK'});
