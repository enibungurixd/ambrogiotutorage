import * as ORM from "sequelize";
import { Sequelize } from 'sequelize';

//export function for Tutor table definiton with Sequelize ORM
export function tutorModel(sequelize: Sequelize){
    return sequelize.define('Tutor', {
        tutorID: {
            type: ORM.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        userFK: ORM.INTEGER,
        tutorLevel: ORM.STRING
    },
    {
        timestamps: false,
        tableName: 'Tutor'
    });
}