import * as ORM from "sequelize";
import { Sequelize } from 'sequelize';

//export function for Path table definiton with Sequelize ORM
export function pathModel(sequelize: Sequelize){
    return sequelize.define('Path', {
        pathID: {
            type: ORM.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        pathName: ORM.STRING,
        pathDescription: ORM.STRING
        
    },
    {
        timestamps: false,
        tableName: 'Path'
    });
}