import * as ORM from "sequelize";
import { Sequelize } from 'sequelize';

//export function for Task table definiton with Sequelize ORM
export function taskModel(sequelize: Sequelize){
    return sequelize.define('Task', {
        taskID: {
            type: ORM.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        taskTitle: ORM.STRING,
        taskDescription: ORM.STRING,
        pathFK: ORM.INTEGER,
        userFK: ORM.INTEGER
    },
    {
        timestamps: false,
        tableName: 'Task'
    });
}