import * as ORM from "sequelize";
import { Sequelize } from 'sequelize';

//export function for Role table definiton with Sequelize ORM
export function roleModel(sequelize: Sequelize){
    return sequelize.define('Role', {
        roleID: {
            type: ORM.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        roleName: ORM.STRING
    },
    {
        timestamps: false,
        tableName: 'Role'
    });
}