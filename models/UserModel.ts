import * as ORM from "sequelize";
import { Sequelize } from 'sequelize';

//export function for User table definiton with Sequelize ORM
export function userModel(sequelize: Sequelize){
    return sequelize.define('User', {
      
        userID: {
            type: ORM.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: ORM.STRING,
        lastname: ORM.STRING,
        password:{
            type: ORM.STRING,
            allowNull: false
        },
    
        email:{
            type: ORM.STRING,
            unique: true,
            allowNull: false
        },
        roleFK: {
            type: ORM.INTEGER, 
            allowNull: false
        },
        pathFK: ORM.INTEGER,
        imageName: ORM.STRING
    },
    {
        timestamps: false,
        tableName: 'User'
    });
}