import * as ORM from "sequelize";
import { Sequelize } from 'sequelize';

//export function for Student table definiton with Sequelize ORM
export function studentModel(sequelize: Sequelize){
    return sequelize.define('Student', {
        studentID: {
            type: ORM.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        studentLevel: ORM.STRING,
        userFK: ORM.INTEGER
    },
    {
        timestamps: false,
        tableName: 'Student'
    });
}