import * as ORM from "sequelize";
import { Sequelize } from 'sequelize';

//export function for Report table definiton with Sequelize ORM
export function reportModel(sequelize: Sequelize){
    return sequelize.define('Report', {
        reportID: {
            type: ORM.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        reportTitle: ORM.STRING,
        reportDate: ORM.DATE,
        reportContent: ORM.STRING,
        userFK: ORM.INTEGER
    },
    {
        timestamps: false,
        tableName: 'Report'
    });
}