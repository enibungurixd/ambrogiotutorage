import {
    Request,
    Response
} from "express";
import {
    NextFunction
} from "connect";
import {
    UserModel,
    PathModel,
    ReportModel,
    RoleModel,
    StudentModel,
    TutorModel,
    TaskModel
} from '../models/model';
import * as bcrypt from 'bcrypt';
import * as generator from 'generate-password';
import * as jwt from 'jsonwebtoken';
import * as sgMail from '@sendgrid/mail';
import * as fs from 'fs';
import * as path from 'path';
import * as multer from 'multer';
import { keys } from '../keys/keys';
const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null,'./uploads/')
    },
    filename: function(req, file, cb){
        cb(null, file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
        cb(null, true);   
    }else{
    cb(null, false);
   }
}

const upload = multer({storage, limits:{fileSize: 1024*1024*7}}, fileFilter);
// const upload = multer({dest: path.join(__dirname, '..', '..', 'uploads')});

export class Routes {
    public routes(app): void {
          
        //GET API for courses, get each course by id in the URL 
        app.get("/api/courses/:id", async (req: Request, res: Response, next: NextFunction) => {

            const id = req.params.id;

            const paths: any = await PathModel.findOne({
                attributes: ['pathID', 'pathName', 'pathDescription'],
                where: {
                    pathID: id
                }
            });
            if (!paths)
                res.status(400).send("This path doesn't exist!");
            else
                res.status(200).send([paths]);
        });

        //GET API for users, get each User by id in the URL
        app.get('/api/usr/:id', async (req: Request, res: Response, next: NextFunction) => {

            const id = req.params.id;
            const attr = ['userID', 'name', 'lastname', 'email', 'pathFK'];

            const user: any = await UserModel.findOne({
                attributes: attr,
                include: [{
                    model: RoleModel,
                    attributes: ['roleName'],
                    as: 'role'
                }],
                where: {
                    userID: id
                }
            });
            if (!user) {
                res.status(400).send("not found!");
            } else {
                res.status(200).send(user);
            }
        });
        app.get('/api/usr1/:id', async (req: Request, res: Response, next: NextFunction) => {

            const id = req.params.id;
            const attr = ['userID', 'name', 'lastname', 'email', 'pathFK', 'imageName'];

            const user: any = await UserModel.findOne({
                attributes: attr,
                include: [{
                    model: RoleModel,
                    attributes: ['roleName'],
                    as: 'role'
                }],
                where: {
                    userID: id
                }
            });
            if (!user) {
                res.status(400).send("not found!");
            } else {
                user.imageName = "http://192.168.10.55:3000/" + user.imageName 
                res.status(200).send(user);
            }
        });

        //POST API for login: 1)checking if user exists 2)comparing input password with db passw 3)Sending token
        app.post('/api/login', async (req: Request, res: Response, next: NextFunction) => {
            //Find one user corresponding the email input
            const user: any = await UserModel.findOne({
                where: {
                    email: req.body.email
                }
            });
            //If this user doesn't exist send JSON with auth: false and the message below.
            if (!user) {
                res.send({
                    auth: false,
                    message: 'Email or password is incorrect!'
                });
            } else {
            //Else if the input email corresponds with the db user email, then we compare the input password with the hashed password in the DB
                const match = await bcrypt.compare(req.body.password, user.password);
            //if passwords and email match send token as a JSON response with auth: true.
                if (match) {
                    const token = jwt.sign({
                        user: user
                    }, keys.jwtSecret, {
                            expiresIn: keys.tokenExpireTime
                        });
                    res.status(200).send({
                        auth: true,
                        token
                    });
                } else {
                //Else passwords don't match, we send JSON with auth: false and the message below
                    res.send({
                        auth: false,
                        message: "Email or password is incorrect!"
                    });
                }
            }
        });

        app.post('/api/login1', async (req: Request, res: Response, next: NextFunction) => {
            //Find one user corresponding the email input
            const user: any = await UserModel.findOne({
                where: {
                    email: req.body.email
                }
            });
            //If this user doesn't exist send JSON with auth: false and the message below.
            if (!user) {
                res.send({
                    auth: false,
                    message: 'Email or password is incorrect!'
                });
            } else {
                // user.imageName = "http://192.168.10.55:3000/" + user.imageName 
            //Else if the input email corresponds with the db user email, then we compare the input password with the hashed password in the DB
                const match = await bcrypt.compare(req.body.password, user.password);
            //if passwords and email match send token as a JSON response with auth: true.

                if (match) {
                    const token = jwt.sign({
                        user  
                    }, keys.jwtSecret, {
                            expiresIn: keys.tokenExpireTime
                        });
                    res.status(200).send({
                        auth: true,
                        token
                    });
                } else {
                //Else passwords don't match, we send JSON with auth: false and the message below
                    res.send({
                        auth: false,
                        message: "Email or password is incorrect!"
                    });
                }
            }
        });

        //GET API for all courses
        app.get('/api/courses', async (req: Request, res: Response, next: NextFunction) => {

            const courses = await PathModel.findAll()
            if (!courses)
                res.status(400).send("notFound");
            else
                res.status(200).send(courses);
        });

        //POST API for creating reports and saving them in the DB
        app.post('/api/post/reports', async (req: Request, res: Response, next: NextFunction) => {
            
            const reportData = {
                reportID: req.body.reportID,
                reportTitle: req.body.reportTitle,
                reportDate: new Date(),
                reportContent: req.body.reportContent,
                userFK: req.body.userFK
            }
            console.log(req.body);
            console.log(reportData);

            const report: any = await ReportModel.create(reportData)
            if (!report)
                res.status(400).send("Report not created!");
            else
                res.status(200).send(report);
        });

        //GET API for all reports
        app.get('/api/reports', async (req: Request, res: Response, next: NextFunction) => {
            const reports: any = await ReportModel.findAll()
            if (!reports)
                res.status(400).send("No reports found!");
            else
                res.status(200).send(reports);
        });

        //GET API for all reports of the tutors
        app.get('/api/tutor/reports', async (req: Request, res: Response, next: NextFunction) => {

            const attr = ['reportID', 'reportTitle', 'reportDate', 'reportContent', 'userFK'];
            const reports: any = await ReportModel.findAll({
                attributes: attr,
                include: [{
                    model: UserModel,
                    attributes: ['userID', 'name', 'lastname', 'roleFK'],
                    as: 'report',
                    where: {
                        roleFK: 2
                    }
                }]
            });

            if (!reports)
                res.status(400).send("notFound");
            else
                res.status(200).send(reports);
        });

        //GET API for all reports of the students
        app.get('/api/student/reports', async (req: Request, res: Response, next: NextFunction) => {
            const attr = ['reportID', 'reportTitle',  'reportDate', 'reportContent'];
            const reports: any = await ReportModel.findAll({
                attributes: attr,
                include: [{
                    model: UserModel,
                    attributes: ['userID', 'name', 'lastname', 'roleFK'],
                    as: 'report',
                    where: {
                        roleFK: 3
                    }
                }]
            });
            if (!reports)
                res.status(400).send("notFound");
            else
                res.status(200).send(reports);
        });

        //GET API for all reports of the backend students
        app.get('/api/student/reports/backend', async (req: Request, res: Response, next: NextFunction) => {

            const attr = ['reportID', 'reportTitle', 'reportDate', 'reportContent'];
            const reports: any = await ReportModel.findAll({
                attributes: attr,
                include: [{
                    model: UserModel,
                    attributes: ['userID', 'name', 'lastname', 'roleFK', 'pathFK'],
                    as: 'report',
                    where: {
                        roleFK: 3,
                        pathFK: 1
                    }
                }]
            });
            if (!reports)
                res.status(400).send("notFound");
            else
                res.status(200).send(reports);
        });

        //GET API for all reports of the frontend students
        app.get('/api/student/reports/frontend', async (req: Request, res: Response, next: NextFunction) => {

            const attr = ['reportID', 'reportTitle', 'reportDate', 'reportContent'];
            const reports: any = await ReportModel.findAll({
                attributes: attr,
                include: [{
                    model: UserModel,
                    attributes: ['userID', 'name', 'lastname', 'roleFK', 'pathFK'],
                    as: 'report',
                    where: {
                        roleFK: 3,
                        pathFK: 2
                    }
                }]

            });
            if (!reports)
                res.status(400).send("notFound");
            else
                res.status(200).send(reports);
        });

        //GET API for all reports of the android students
        app.get('/api/student/reports/android', async (req: Request, res: Response, next: NextFunction) => {

            const attr = ['reportID', 'reportTitle','reportDate', 'reportContent'];
            const reports: any = await ReportModel.findAll({
                attributes: attr,
                include: [{
                    model: UserModel,
                    attributes: ['userID', 'name', 'lastname', 'roleFK', 'pathFK'],
                    as: 'report',
                    where: {
                        roleFK: 3,
                        pathFK: 3
                    }
                }]

            });
            if (!reports)
                res.status(400).send("notFound");
            else
                res.status(200).send(reports);
        });

        //GET API for all reports of the iOS students
        app.get('/api/student/reports/ios', async (req: Request, res: Response, next: NextFunction) => {

            const attr = ['reportID', 'reportTitle', 'reportDate', 'reportContent'];
            const reports: any = await ReportModel.findAll({
                attributes: attr,
                include: [{
                    model: UserModel,
                    attributes: ['userID', 'name', 'lastname', 'roleFK', 'pathFK'],
                    as: 'report',
                    where: {
                        roleFK: 3,
                        pathFK: 4
                    }
                }]
            });
            if (!reports)
                res.status(400).send("notFound");
            else
                res.status(200).send(reports);
        });

        //GET API for tasks, get task by id in the URL
        app.get('/api/tasks/:id', async (req: Request, res: Response, next: NextFunction) => {
            const id = req.params.id;

            const task: any = await TaskModel.findOne({
                attributes: ["taskTitle", "taskDescription", "taskID", "pathFK", "userFK"],
                include: [{
                    model: PathModel,
                    attributes: ["pathID", "pathName"],
                    as: "path",
                }],
                where: {
                    taskID: id
                }
            });
            if (!task)
                res.status(400).send("Task not found!");
            else
                res.status(200).send(task);
        });

        //GET API for tasks, get task by pathID in the URL
        app.get('/api/m/tasks/:id', async (req: Request, res: Response, next: NextFunction) => {
            const id = req.params.id;

            const tasks: any = await TaskModel.findAll({
                attributes: ["taskTitle", "taskDescription", "taskID",  "pathFK", "userFK"],
                include: [{
                    model: PathModel,
                    attributes: ["pathID", "pathName"],
                    as: "path",
                    where: {
                        pathID: id
                    }
                }]
            });
            if (!tasks)
                res.status(400).send("Task not found!");
            else
                res.status(200).send(tasks);
        });

        //GET API for reports, get all reports by user id in the URL
        app.get('/api/reports/:id', async (req: Request, res: Response, next: NextFunction) => {

            const id = req.params.id;
            const reports: any = await ReportModel.findAll({
                attributes: ['reportTitle', 'reportDate', 'reportContent', 'reportID', 'userFK'],
                include: [{
                    model: UserModel,
                    attributes: ['name', 'lastname', 'roleFK', 'pathFK'],
                    as: 'report'
                }],
                where: {
                    userFK: id
                }
            });

            if (!reports)
                res.status(400).send("No reports found!");
            else
                res.status(200).send(reports);
        });

        //GET API for reports, get report by id in the URL
        app.get('/reports/:id', async (req: Request, res: Response, next: NextFunction) => {

            const id = req.params.id;
            const report: any = await ReportModel.findOne({
                where: {
                    reportID: id
                }
            })
            if (!report)
                res.status(400).send("No reports found!");
            else
                res.status(200).send([report]);
        });

        //POST API for creating tasks
        app.post("/api/create/task", async (req: Request, res: Response, next: NextFunction) => {

            const task: any = await TaskModel.create({
                taskID: req.body.taskID,
                taskTitle: req.body.taskTitle,
                taskDescription: req.body.taskDescription,
                pathFK: req.body.pathFK,
                userFK: req.body.userFK
            });
            if (!task)
                res.status(400).send("Task not created");
            else
                res.status(200).send(task);
        });

        //GET API for all backend tasks
        app.get('/api/task/backend', async (req: Request, res: Response, next: NextFunction) => {

            const task: any = await TaskModel.findAll({
                attributes: ["taskTitle", "taskDescription", "taskID",  "pathFK"],
                include: [{
                    model: PathModel,
                    attributes: ["pathID", "pathName"],
                    as: "path",
                }],
                where: {
                    pathFK: 1
                }

            });
            if (!task)
                res.status(400).send("Task not found!");
            else
                res.status(200).send(task);
        });

        //GET API for all tasks including courses that belong to each task.
        app.get('/api/tasks/', async (req: Request, res: Response, next: NextFunction) => {

            const tasks: any = await TaskModel.findAll({
                attributes: ["taskTitle", "taskDescription", "taskID", "pathFK"],
                include: [{
                    model: PathModel,
                    attributes: ["pathID", "pathName"],
                    as: "path",
                }]
            });
            if (!tasks)
                res.status(400).send("Task not found!");
            else
                res.status(200).send({
                    data: tasks
                });
        });

        //GET API for all frontend tasks
        app.get('/api/task/frontend', async (req: Request, res: Response, next: NextFunction) => {

            const tasks: any = await TaskModel.findAll({
                attributes: ["taskTitle", "taskDescription", "taskID","pathFK"],
                include: [{
                    model: PathModel,
                    attributes: ["pathID", "pathName"],
                    as: "path",
                }],
                where: {
                    pathFK: 2
                }
            });
            if (!tasks)
                res.status(400).send("Task not found!");
            else
                res.status(200).send(tasks);
        });

        //GET API for all android tasks
        app.get('/api/task/android', async (req: Request, res: Response, next: NextFunction) => {

            const tasks: any = await TaskModel.findAll({
                attributes: ["taskTitle", "taskDescription", "taskID", "pathFK"],
                include: [{
                    model: PathModel,
                    attributes: ["pathID", "pathName"],
                    as: "path",
                }],
                where: {
                    pathFK: 3
                }

            });
            if (!tasks)
                res.status(400).send("Task not found!");
            else
                res.status(200).send(tasks);
        });

        //GET API for all iOS tasks
        app.get('/api/task/ios', async (req: Request, res: Response, next: NextFunction) => {

            const tasks: any = await TaskModel.findAll({
                attributes: ["taskTitle", "taskDescription", "taskID",  "pathFK"],
                include: [{
                    model: PathModel,
                    attributes: ["pathID", "pathName"],
                    as: "path",
                }],
                where: {
                    pathFK: 4
                }

            });
            if (!tasks)
                res.status(400).send("Task not found!");
            else
                res.status(200).send(tasks);
        });

        //GET API for all users with role Student
        app.get('/api/students', async (req: Request, res: Response, next: NextFunction) => {

            const attr = ['userID', 'name', 'lastname', 'email', 'roleFK', 'pathFK'];

            const users: any  = await UserModel.findAll({
                attributes: attr,
                include: [{
                    model: PathModel,
                    attributes: ['pathName'],
                    as: 'course',
                }],
                where: {
                    roleFK: 3
                }
            });
            if (!users) {
                res.status(400).send("not found!");
            } else {
                res.status(200).send(users);
            }
        });

        //GET API for all users with role Tutor
        app.get('/api/tutors', async (req: Request, res: Response, next: NextFunction) => {

            const attr = ['userID', 'name', 'lastname', 'email', 'roleFK', 'pathFK'];

            const users: any = await UserModel.findAll({
                attributes: attr,
                include: [{
                    model: PathModel,
                    attributes: ['pathName'],
                    as: 'course',
                }],
                where: {
                    roleFK: 2
                }
            });

            if (!users) {
                res.status(400).send("not found!");
            } else {
                res.status(200).send(users);
            }
        });

        app.get('/api/tutors1', async (req: Request, res: Response, next: NextFunction) => {

            const attr = ['userID', 'name', 'lastname', 'email', 'roleFK', 'imageName', 'pathFK'];

            const users: any = await UserModel.findAll({
                attributes: attr,
                include: [{
                    model: PathModel,
                    attributes: ['pathName'],
                    as: 'course',
                }],
                where: {
                    roleFK: 2
                }
            });
            // for(var i = 0; i<users.length; i++){
            
            //     users.imageName = "http://192.168.10.55:3000/" + users.imageName;
            
            // }
            if (!users) {
                res.status(400).send("not found!");
            } else {
                for(var i = 0; i<users.length; i++){
            
                    users[i].imageName = "http://192.168.10.55:3000/" + users[i].imageName;
                
                }
                res.status(200).send(users);
            }
        });
       
        app.get('/api/students1', async (req: Request, res: Response, next: NextFunction) => {

            const attr = ['userID', 'name', 'lastname', 'email', 'roleFK', 'imageName', 'pathFK'];

            const users: any = await UserModel.findAll({
                attributes: attr,
                include: [{
                    model: PathModel,
                    attributes: ['pathName'],
                    as: 'course',
                }],
                where: {
                    roleFK: 3
                }
            });
            if (!users) {
                res.status(400).send("not found!");
            } else {
                for(var i = 0; i<users.length; i++){
            
                    users[i].imageName = "http://192.168.10.55:3000/" + users[i].imageName;
                
                }
                res.status(200).send(users);
            }
        });

        //POST API for admin's dashboard to register new users. 
        app.post('/api/register', async (req: Request, res: Response, next: NextFunction) => {
            console.log("Status: ", res.statusCode);

            //password generator
            const genPass = await generator.generate({
                length: 10,
                numbers: true
            });
            
            //SendGrid configuration
            sgMail.setApiKey(keys.sendGridKey);

            //user data coming from body with including the generated password
            const userData = {
                userID: req.body.userID,
                name: req.body.name,
                lastname: req.body.lastname,
                password: genPass,
                email: req.body.email,
                roleFK: req.body.roleFK,
                pathFK: req.body.pathFK
            }

            //email body template from frontEnd, edit to include the generated password.
            let htmlPath = path.join(__dirname, 'forEmail', 'index.html')
            const file = fs.readFileSync(htmlPath, 'utf8');
            let beggining = file.slice(0, 637 + 58);
            let slicePass = file.slice(637 + 58);
            let ps = '';
            ps = ps + beggining + userData.password + slicePass;

            //email configuration, for html I assigned the body template with the password
            const msg = {
                to: userData.email,
                from: 'eni.bunguri@gmail.com',
                subject: 'Generated Password',
                html: ps
            };
            console.log(req.body);
            console.log(userData);
            console.log(userData.password);

            //Existing user?
            const user = await UserModel.findOne({
                where: {
                    email: req.body.email
                }
            })
            if (user) {

                res.send("This email is already registered");
                res.end();

            } else {
                //hash the generated password
                const hashedPassword = await bcrypt.hash(userData.password, 12)
                //save the userData in the DB with the hashed password
                const user: any = await UserModel.create({
                    userID: req.body.userID,
                    name: req.body.name,
                    lastname: req.body.lastname,
                    password: hashedPassword,
                    email: req.body.email,
                    roleFK: req.body.roleFK,
                    pathFK: req.body.pathFK,
                    imageName: req.body.imageName
                });
                if (user) {
                    sgMail.send(msg);
                    res.send("User registered successfully!");
                } else if (userData.roleFK == 2) {
                    //populating Tutor table
                    const tutor = await TutorModel.create({
                        tutorID: req.body.tutorID,
                        userFK: req.body.userID,
                        tutorLevel: req.body.tutorLevel
                    });
                } else if (userData.roleFK == 3) {
                    //populating student table
                    const student = await StudentModel.create({
                        studentID: req.body.studentID,
                        studentLevel: req.body.studentLevel,
                        userFK: req.body.userID
                    });
                } else {
                    res.send("User not registered!");
                }
            }

        });

        //POST API for Admin to users
        app.post('/api/admin/edit', async (req: Request, res: Response, next: NextFunction) => {
            const user: any = await UserModel.findOne({
                where: {
                    userID: req.body.userID
                }
            });

            const edited = await user.update({
                roleFK: req.body.roleFK,
                pathFK: req.body.pathFK
            });
            if (!edited)
                res.send({
                    message: "The editing was not successful"
                });
            else
                res.send({
                    message: "The editing was successful"
                });
        });

        //POST API for user changing password or name or lastname or all of them.
        app.post('/api/edit/user', async (req:  Request, res: Response, next: NextFunction) => {

            const user: any = await UserModel.findOne({
                where: {
                    userID: req.body.userID
                }
            });
            const compare = await bcrypt.compare(req.body.password, user.password);

            if (!compare) {
                res.send("Password is incorrect!");

            } else {

                const pass = req.body.newPassword;
                const bcrypted = await bcrypt.hash(pass, 12);
                const edited = await user.update({
                    password: bcrypted,
                    name: req.body.name,
                    lastname: req.body.lastname
                });

                if (edited) {
                    res.send("Profile edit was successful!");
                } else {
                    res.status(400).send("Profile edit failed!");
                }
            }
        });

        //DELETE API for deleting user by id in the url and all its reports
        app.delete('/api/delete/user/:id', async(req: Request, res: Response, next: NextFunction) => {
            let id = req.params.id;
            const user: any = await UserModel.findOne({
                where: {
                    userID: id
                }
            });
            const deleted = await user.destroy();
            if (deleted) {
                res.send("User and its reports are deleted successfully");
            } else {
                res.send("user not deleted");
            }
        });
        
        app.post('/api/avatar', upload.single('file'), async(req, res, next)=>{
            console.log('body: ',req.body);
            console.log('file: ',req.file);
            const user: any = await UserModel.findOne({
                where: {
                    userID: req.body.userID
                }
            });
            if(!user){
                res.status(404).send({message: 'user does not exist!'});
            }else{                
                if(!req.file)
                res.status(403).send({message: "This type of file is not allowed to upload!"})
                else{
                console.log(req.body, 'Body'); 
                console.log(req.file);
                const edited = await user.update({
                imageName: req.file.originalname
                });
                if(!edited){
                    res.status(400).send({message: "Update request not successful!"});
                }
                else{
                res.status(200).send({message: "Success!"});
                }
             }
            }
        });
    }
}
