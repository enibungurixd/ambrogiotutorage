import * as express from 'express';
import * as bodyParser from "body-parser";
import { Routes } from "./routes/crmRoutes";
import * as errorhandler from 'strong-error-handler';
import * as cors from 'cors';
import * as path from 'path';

class App {
    //Declaring types before wiring up
    public app: express.Application;
    public routePrv: Routes = new Routes();
    public PORT: any = process.env.PORT || 3000;
    public saltRounds: 10;
    public options:cors.CorsOptions = {
        allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
        credentials: true,
        methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
        preflightContinue: false
      };
      
    //constructor declaration: express app initialization, configuration function call, wiring routes to the express app  
    constructor() {
        this.app = express();
        this.config();        
        this.routePrv.routes(this.app);     
    }
    
    //private configuration function: bodyParser middleware, cors middleware, (req, res, next) Headers middlware, errorHandler for 'test' and 'dev' environment.
    private config(): void{
        this.app.use(express.static(path.join(__dirname, '..', 'uploads')));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.listen(this.PORT);
        this.app.use(cors(this.options));
        this.app.use((req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Expose-Headers", "x-total-count");
            res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH");
            res.header("Access-Control-Allow-Headers", "Content-Type,authorization");
          
            next();
          });
        this.app.use(errorhandler({
            debug: process.env.ENV !== 'prod',
            log: true,
          }));
          
       
    }
}

//exporting the app object
export default new App().app;